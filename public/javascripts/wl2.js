var wl2 = angular.module('wl2', ['ngRoute', 'ngSanitize', 'wl2Filters']);
wl2.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
  $routeProvider
    .when('/', {templateUrl: '/index?angular=1', controller: IndexCtrl, reloadOnSearch: false})
    .when('/help', {templateUrl: '/help?angular=1', controller: HelpCtrl, reloadOnSearch: false})
    .when('/log', {templateUrl: '/log?angular=1', controller: LogCtrl, reloadOnSearch: false})
    .when('/!:name', {templateUrl: function(p){ return '/!'+p.name+'?angular=1'; }, controller: ProfileCtrl})
    .when('/!:name/:message', {templateUrl: function(p){ return '/!'+p.name+'/'+p.message+'?angular=1'; }, controller: SingleCtrl})
    .when('/settings', {templateUrl: '/settings?angular=1', controller: SettingsCtrl})
    .when('/auth/:strategy', {redirectTo: function(){ location.href = location.href; return false; }})
    .when('/connect', {templateUrl: '/connect?angular=1', controller: ConnectCtrl})
    .when('/logout', {redirectTo: function(){ location.href = location.href; return false; }})
    .otherwise({redirectTo:'/'});
  $locationProvider.html5Mode(true);
}]);

function messageSetup(messageData, $rootScope, $sanitize, $filter){
  if ($rootScope.user && messageData.user.name == $rootScope.user.name){
    messageData.me = 'me';
  }
  if (messageData.tags.length){
    messageData.tagsLink = '/?t='+encodeURIComponent(wl2c.encodeBase64Tag(messageData.tags));
  }
  messageData.messageHtml = $sanitize($filter('markdown')(messageData.message));
  messageData.createdFormatted = $sanitize($filter('date')(messageData.created, 'yyyy-MM-dd HH:mm:ss'));
}

function IndexCtrl($scope, $rootScope, $routeParams, $http, $location, $timeout, $filter, $sanitize){
  $scope.postform = {
    message: '',
    file: null,
    tags: []
  };

  var previewTimer = null;
  $scope.$watch('postform.message', function(){
    if (previewTimer){
      $timeout.cancel(previewTimer);
    }
    previewTimer = $timeout(function(){
      previewTimer = null;
      $scope.messagePreview = $sanitize($filter('markdown')($scope.postform.message));
    }, 100);
  });

  function createEditor(){
    var e = document.getElementById('message');
    if (!e) return;
    e.onkeydown = function(e){
      if (e.keyCode == 13){
        var enterSubmit = localStorage.enterType == "1";
        if (enterSubmit != e.shiftKey){
          $scope.$apply(function(){
            $scope.post();
          });
          e.preventDefault();
        }
      }
    };
    e.onfocus = function(e){
      $scope.$apply(function(){
        $scope.messageFocus = 'active';
      });
    };
    e.onblur = function(e){
      $scope.$apply(function(){
        $scope.messageFocus = '';
      });
    };
  }
  function destroyEditor(){
    var e = document.getElementById('message');
    if (!e) return;
    e.onkeydown = null;
    e.onfocus = null;
    e.onblur = null;
  }

  var eventSource;
  function createEventSource(tags){
    $scope.messages = [];
    var for3G = localStorage.networkType == "1";
    eventSource = new EventSource(
      '/message/stream?t='+encodeURIComponent(wl2c.encodeBase64Tag(tags))+
      (for3G ? '&3g=1' : '')
    );
    eventSource.addEventListener('message', function(e){
      if (!e.data) return;
      $scope.$apply(function(){
        var jsons = e.data.split('\n');
        jsons.forEach(function(data){
          var messageData = JSON.parse(data);
          messageSetup(messageData, $rootScope, $sanitize, $filter);
          $scope.messages.unshift(messageData);
        });
        //updateSending();
        for(var i = $scope.messages.length - 50; i > 0; --i){
          $scope.messages.pop();
        }
      });
    });
    eventSource.addEventListener('score', function(e){
      if (!e.data) return;
      $scope.$apply(function(){
        var messageData = JSON.parse(e.data);
        for(var i = 0; i < $scope.messages.length; ++i){
          if ($scope.messages[i]._id == messageData._id){
            $scope.messages[i].score = messageData.score;
            break;
          }
        }
      });
    });
    eventSource.addEventListener('revealed', function(e){
      if (!e.data) return;
      $scope.$apply(function(){
        var messageData = JSON.parse(e.data);
        for(var i = 0; i < $scope.messages.length; ++i){
          if ($scope.messages[i]._id == messageData._id){
            $scope.messages[i].score = messageData.score;
            $scope.messages[i].ninja = messageData.ninja;
            $scope.messages[i].user = messageData.user;
            break;
          }
        }
      });
    });
    eventSource.onerror = function(e){
      if (eventSource.readyState == 2){
        console.log('通信が切断されました（サーバ側の都合かも？）。自動リロードします');
        eventSource.close();
        setTimeout(function(){
          location.reload(true);
        }, 1000+Math.random()*3000);
      }
    };
  }

  function destroyEventSource(){
    if (eventSource){
      eventSource.close();
      eventSource = null;
    }
  }

  function init(){
    $scope.postform.tags = wl2c.decodeBase64Tag($routeParams.t);
    destroyEventSource();
    createEventSource($scope.postform.tags);

    destroyEditor();
    createEditor();
  }

  function destroy(){
    destroyEditor();
    destroyEventSource();
  }

  $scope.$on("$routeUpdate", init);
  $scope.$on("$destroy", destroy);
  init();

 $scope.relocate = function(){
    var old = wl2c.decodeBase64Tag($routeParams.t);
    var current = wl2c.encodeBase64Tag($scope.postform.tags);
    if (old != current){
      $location.search($scope.postform.tags.length ? {t: current} : {});
    }
  };

  $scope.incScore = function(message, pt){
    $http({
      method: 'POST',
      url: '/message/score',
      data: {id: message._id, pt: pt}
    }).success(function(data, status, headers, config){
      message.score = data.score;
      if (data.ninja){
        message.ninja = data.ninja;
        message.user = data.user;
      }
    }).error(function(data, status, headers, config){
      alert(data);
    });
  };

  $scope.canUseFileApi = function(){
    return !!window.File;
  };
  $scope.selectFile = function(elem){
    if (!$scope.canUseFileApi()) return;
    $scope.$apply(function(){
      $scope.file = elem.files[0];
      angular.element(elem).replaceWith('<input type="file" onchange="angular.element(this).scope().selectFile(this)">');
    });
  };
  $scope.post = function(pt){
    if ($scope.postform.message.replace(/[\s\r\n\xa0]/g, '') == '') return;

    $scope.busy = true;

    //本文内からタグを検索
    var splitted = wl2c.findTagsFromString($scope.postform.message), tags = [];
    for(var i = 1; i < splitted.length; i += 2){
      tags.push(splitted[i].substring(1));
    }
    Array.prototype.unshift.apply($scope.postform.tags, tags);
    $scope.relocate();

    var payload = new FormData();
    payload.append('body', JSON.stringify($scope.postform));
    if ($scope.file){
      payload.append('file', $scope.file);
    }
    if (pt){
      payload.append('pt', pt);
    }
    $http({
      method: 'POST',
      url: '/message/post',
      data: payload,
      headers: { 'Content-Type': undefined },
      transformRequest: function(data) { return data; }
    }).success(function(data, status, headers, config){
      $scope.busy = false;
    }).error(function(data, status, headers, config){
      $scope.busy = false;
      alert('unknown error');
    });

    $scope.postform.message = '';
    $scope.file = null;
  };
}

function ProfileCtrl($scope, $rootScope, $routeParams, $http, $filter, $sanitize, $templateCache, $route){
  $templateCache.remove($route.current.loadedTemplateUrl);

  $scope.messages = [];
  $http({method: 'GET', url: '/message/log', params: {user: $routeParams.name}}).success(function(data, status, headers, config){
    for(var i = 0, message; i < data.messages.length; ++i){
      message = data.messages[i];
      messageSetup(message, $rootScope, $sanitize, $filter);
    }
    $scope.messages = data.messages;
  });
}

function SingleCtrl($scope, $templateCache, $route, $rootScope, $sanitize, $filter){
  $templateCache.remove($route.current.loadedTemplateUrl);
  messageSetup($scope.messages[0], $rootScope, $sanitize, $filter);
}

function HelpCtrl($scope, $location, $routeParams){
  function init(){
    var tabName = $routeParams.tab ? $routeParams.tab : 'help-wl2';
    $('#help-tab a[data-target=#'+tabName+']').tab('show');
  }
  $scope.$on("$routeUpdate", init);
  init();
}

function LogCtrl($scope, $rootScope, $routeParams, $http, $filter, $location, $sanitize){
  $scope.messages = [];
  $scope.hasPrev = false;
  $scope.hasNext = false;
  $scope.page = 1;
  $scope.from = new Date();
  $scope.to = new Date();

  $('#calendar').DatePicker({
    mode: 'range',
    inline: true,
    calendars: 1,
    onChange: function(dates){
      $scope.$apply(function(){
        $scope.from = dates[0];
        $scope.to = dates[1];
      });
    }
  });

  function yyyymmdd(date){
    return $filter('date')(date, 'yyyy-MM-dd');
  }

  function init(){
    $scope.msgs = [];
    $scope.tags = wl2c.decodeBase64Tag($routeParams.t);
    $scope.from = new Date($routeParams.from ? $routeParams.from : new Date());
    $scope.to = new Date($routeParams.to ? $routeParams.to : new Date());
    $('#calendar').DatePickerSetDate([$scope.from, $scope.to]);

    $http({method: 'GET', url: '/message/log', params: $routeParams}).success(function(data, status, headers, config){
      for(var i = 0, message; i < data.messages.length; ++i){
        message = data.messages[i];
        messageSetup(message, $rootScope, $sanitize, $filter);
      }
      $scope.messages = data.messages;
      $scope.page = parseInt($routeParams.p);
      if (isNaN($scope.page)){
        $scope.page = 1;
      }
      $scope.hasPrev = $scope.page > 1;
      $scope.hasNext = !!data.nextId;
    });
  }

  function destroy(){
  }

  $scope.$on("$routeUpdate", init);
  $scope.$on("$destroy", destroy);
  init();

  $scope.addTag = function(tag){
    if (!tag) return;
    $scope.tags.push(tag.replace(/^[#＃]/, ''));
    $scope.tags = wl2c.uniqueAndLimitTags($scope.tags);
  };

  $scope.find = function(page){
    $location.search({
      from: yyyymmdd($scope.from),
      to: yyyymmdd($scope.to),
      p: page,
      t: wl2c.encodeBase64Tag($scope.tags)
    });
  };
}

function SettingsCtrl($scope, $rootScope, $http, $templateCache, $route){
  $templateCache.remove($route.current.loadedTemplateUrl);

  $scope.enterType = localStorage.enterType == "1" ? "1" : "0";
  $scope.networkType = localStorage.networkType == "1" ? "1" : "0";

  $scope.canUseFileApi = function(){
    return !!window.File;
  };
  $scope.selectFile = function(elem){
    if (!$scope.canUseFileApi()) return;
    $scope.$apply(function(){
      $scope.userform.avatar = elem.files[0];
      angular.element(elem).replaceWith('<input type="file" onchange="angular.element(this).scope().selectFile(this)">');
    });
  };
  $scope.save = function(){
    $scope.busy = true;
    var payload = new FormData();
    for(var key in $scope.userform){
      payload.append(key, $scope.userform[key]);
    }
    $http({
      method: 'POST',
      url: '/settings/save',
      data: payload,
      headers: { 'Content-Type': undefined },
      transformRequest: function(data) { return data; }
    }).success(function(data, status, headers, config){
      $scope.busy = false;
      $scope.userform.avatar = null;
      $rootScope.user = data;
      localStorage.enterType = $scope.enterType;
      localStorage.networkType = $scope.networkType;
    }).error(function(data, status, headers, config){
      $scope.busy = false;
      alert('could not save your settings.');
    });
  };
}

function ConnectCtrl($scope, $rootScope, $http, $templateCache, $route){
  $templateCache.remove($route.current.loadedTemplateUrl);
}
