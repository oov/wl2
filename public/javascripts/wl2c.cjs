(function(root){
var atob, btoa;
var htmlparser;
if (!root.atob || !root.btoa){
	if (typeof require !== 'undefined'){
		atob = require('atob');
		btoa = require('btoa');
		//htmlparser = require('./htmlparser.js');
	} else {
		throw 'atob and btoa not found';
	}
} else {
	atob = root.atob;
	btoa = root.btoa;
	//htmlparser = root.htmlparser;
}

function html2textMessage(html){
	var root = {name: 'root', parentNode: null, childNodes:[]}, current = root;
	htmlparser.htmlParser(html, {
		start: function(tag, attrs, unary) {
			current.childNodes.push(current = {tag: tag, parentNode: current, childNodes: [], hash: tag == 'span' && attrs['data-hash']});
		},
		end: function(tag) {
			current = current.parentNode;
		},
		chars: function(text) {
			current.childNodes.push(text);
		},
		comment: function(text) {}
	});

	var output = [], s = '', nl = false, sp = false;
	function n(node){
		if (htmlparser.blockElements[node.tag]){
			nl = true;
		}
		if (node.hash){
			sp = true;
		}
		for(var i = 0; i < node.childNodes.length; ++i){
			var cur = node.childNodes[i];
			if (typeof cur == 'string'){
				if (sp){
					if (!nl && s.charAt(s.length-1).trim() != ''){
						s += ' ';
					}
					sp = false;
				}
				if (nl){
					if (s.replace(/\r|\n/g, '') != ''){
						output.push(s);
					}
					s = '';
					nl = false;
				}
				s += cur;
				continue;
			}
			n(cur);
		}
		if (htmlparser.blockElements[node.tag]){
			nl = true;
		}
		if (node.hash){
			sp = true;
		}
	}
	n(root);
	if (s.replace(/\r|\n/g, '') != ''){
		output.push(s);
	}
	
	s = '';
	for(var i = 0, t; i < output.length; ++i){
		s += output[i].replace(/[\s\r\n\xa0\ufeff]/g, ' ').trim() + '\n';
	}

	return s.trim();
}

function findTagsFromString(str){
    'use strict';
    if (typeof str != 'string') return [''];

    var r = [];
    var findTag = function(str) {
        var hashRE = /(^|\s)((?:#|＃)[^\s#＃]+)(?!#|＃)(?=\s|$)/g, m, lastIndex = 0, r = [];
        while ((m = hashRE.exec(str)) !== null) {
            r.push(str.substring(lastIndex, m.index + m[1].length));
            r.push(m[2]);
            lastIndex = hashRE.lastIndex;
        }
        if (str.length != lastIndex){
            r.push(str.substring(lastIndex));
        }
        return r;
    }

    var codeRE = /```[\s\S]+?```|`[^\r\n]+?`/g,cm,cidx=0,hstr,sippo="",_ft;
    while( (cm = codeRE.exec(str)) !=null ){
        hstr = str.substr( cidx, cm.index - cidx );
        if( hstr.length < 1 ){
            sippo = sippo + cm[0];
            cidx = codeRE.lastIndex;
            continue;
        }
        _ft = findTag(hstr);                    
        _ft[0] = sippo + _ft[0];                
        sippo = _ft.length%2 ? _ft.pop() : "";    
        sippo = sippo + cm[0];                    
        r = r.concat( _ft );                    

        cidx = codeRE.lastIndex;
    }
    if( str.length > cidx ){
        _ft = findTag( str.substr(cidx) );
        _ft[0] = sippo + _ft[0];
        r = r.concat( _ft );
    }else{
        r[r.length] = sippo;
    }

    return r;
}

function uniqueAndLimitTags(tmpTags){
	if (toString.apply(tmpTags) != '[object Array]') return [];
	var tags = [], tagUsed = {}, tagQuery;
	for(var i = 0, tag; i < tmpTags.length && tags.length < 10; ++i){
		tag = tmpTags[i];
		if (typeof tag != 'string' || tag == '') continue;
		tag = tag.replace(/#|\s/g, '_');
		if (!tagUsed[tag]){
			tags.push(tag);
			tagUsed[tag] = 1;
		}
	}
	return tags;
}

function encodeBase64String(str){
	return btoa(unescape(encodeURIComponent(str)));
}

function decodeBase64String(b64){
	return decodeURIComponent(escape(atob(b64)));
}

function encodeBase64Tag(tags){
	return encodeBase64String(tags.join('#'));
}

function decodeBase64Tag(str){
	try{
		return uniqueAndLimitTags(decodeBase64String(str).split('#'));
	} catch(e){
		return [];
	}
}

(function(name, obj){
	var old = root[name];
	// AMD / RequireJS
	if (typeof define !== 'undefined' && define.amd) {
		define(name, [], function(){ return obj; });
	}
	if (typeof module !== 'undefined' && module.exports) {
		module.exports = obj;
	} else {
		root[name] = obj;
	}

  obj.noConflict = function() {
		root[name] = old;
		return obj;
	};
})('wl2c', {
	html2textMessage: html2textMessage,
	findTagsFromString: findTagsFromString,
	uniqueAndLimitTags: uniqueAndLimitTags,
	encodeBase64String: encodeBase64String,
	decodeBase64String: decodeBase64String,
	encodeBase64Tag: encodeBase64Tag,
	decodeBase64Tag: decodeBase64Tag
});

})(this);
