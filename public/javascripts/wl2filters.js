angular.module('wl2Filters', []).filter('markdown', function(){
	var options = {
	  gfm: true,
	  tables: true,
	  breaks: true,
	  pedantic: false,
	  sanitize: true,
	  smartLists: true,
	  langPrefix: '',
	  highlight: function(code, lang) {
	  	lang = (lang || '').toLowerCase();
			if (hljs.LANGUAGES[lang]){
				code = hljs.highlight(lang, code);
			} else if (lang != 'aa') {
				code = hljs.highlightAuto(code);
			} else {
				return code;
			}
			return code.value;
	  }
	};
	var lexer = new marked.Lexer(options);
	var parser = new marked.Parser(options);
	return function(input){
		if (typeof input != 'string') return '';
		var g = lexer.lex(input);
		return parser.parse(g);
	};
});

