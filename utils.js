import fetch from 'node-fetch';
import {fileTypeFromFile} from 'file-type';
import fs from 'node:fs';
import mongoose from 'mongoose';
import os from 'node:os';
import {pipeline} from 'node:stream';
import {promisify} from 'node:util';
import sharp from 'sharp';

const asyncUnlink = promisify(fs.unlink);
const asyncPipeline = promisify(pipeline);

const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_';
export function generateToken(size){
  var ret = [];
  for(var i = 0; i < size; ++i){
    ret.push(chars.charAt(Math.floor(Math.random()*chars.length)));
  }
  return ret.join('');
}

export async function downloadToTempDir(url){
  //適当なファイル名を生成する
  const res = await fetch(url);
  if (!res.ok){
    throw new Error(`failed to download from `+url);
  }
  const fn = os.tmpdir() + '/' + Date.now() + '-' + (Math.random() * 0xffffffff);
  await asyncPipeline(res.body, fs.createWriteStream(fn));
  return fn;
}

export async function writeImage(req, res, id){
  const bucket = new mongoose.mongo.GridFSBucket(mongoose.connection.db);
  const found = await bucket.find({filename: id}, {limit: 1, sort: {uploadDate: -1}}).toArray();
  if (!found.length) {
    res.status(404).send('image not found');
    return;
  }
  const uploadDate = found[0].uploadDate;
  uploadDate.setMilliseconds(0);
  var ims = new Date(req.get('If-Modified-Since'));
  if (ims >= uploadDate){
    res.status(304).send('not modified');
    return;
  }
  res.setHeader('Content-Type', found[0].contentType);
  res.setHeader('Content-Length', found[0].length);
  res.setHeader('Last-Modified', found[0].uploadDate.toUTCString());
  const src = bucket.openDownloadStream(found[0]._id);
  await asyncPipeline(src, res);
}

export async function fileinfo(path) {
  const typ = await fileTypeFromFile(path);
  const fi = {
    path: path,
    mime: typ ? typ.mime : 'application/octet-stream',
  };
  if (['image/jpeg', 'image/png', 'image/gif'].indexOf(fi.mime) != -1){
    const meta = await sharp(path).metadata();
    fi.width = meta.width;
    fi.height = meta.height;
  }
  return fi;
}

export async function remove(path){
  return await asyncUnlink(path);
}

export async function saveAndRemove(path, mime, id){
  const bucket = new mongoose.mongo.GridFSBucket(mongoose.connection.db);
  const src = fs.createReadStream(path);
  const dest = bucket.openUploadStream(id, {"contentType": mime});
  await asyncPipeline(src, dest);
  await asyncUnlink(path);
  return dest.id
}

export async function resize(path, mime, id, size){
  const destPath = path + '_' + size;
  await sharp(path).resize(size, size, {
    fit: 'inside',
    withoutEnlargement: true,
  }).toFile(destPath);
  await saveAndRemove(destPath, mime, id);
}

export async function crop(path, mime, id, size){
  const destPath = path + '_' + size;
  await sharp(path).resize(size, size).toFile(destPath);
  await saveAndRemove(destPath, mime, id);
}
