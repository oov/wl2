import mongoose from 'mongoose';

import {User} from '../models/user.js';
import {Message} from '../models/message.js';

import wl2c from '../public/javascripts/wl2c.cjs';
import * as utils from '../utils.js';

export async function score(req, res){
  if (!req.user){
    res.status(403).send('please login');
    return;
  }
  const verifyPoint = pt => {
    switch(pt){
      case 1: case 5:
      case 10: case 30: case 50:
      case 100:
        return parseInt(pt, 10);
    }
    return 0;
  }
  const pt = verifyPoint(req.body.pt);
  if (!pt) {
    res.status(403).send('could not be processed');
    return;
  }
  const message = await Message.score(
    req.user._id,
    new mongoose.Types.ObjectId(req.body.id),
    pt
  );
  const revealed = message.ninja && message.ninja.point && message.score >= message.ninja.point && message.score - pt < message.ninja.point;
  if (revealed){
    message.notifyRevealed();
    res.json({score: message.score, ninja: message.ninja.point, user: message.toJSONClientUser()});
  } else {
    message.notifyScore();
    res.json({score: message.score});
  }
};

export async function file(req, res){
  const typ = req.params[0];
  const id = req.params[1];
  const message = await Message.findOne({_id: mongoose.Types.ObjectId(id)}).exec();
  if (!message || !message.file || (!message.image && typ == 'thumb')){
    res.status(404).send('file not found');
    return;
  }
  await utils.writeImage(req, res, message._id + (typ == 'thumb' ? '_256' : ''));
};

export async function single(req, res){
  const id = req.params[0];
  const message = await Message.findOne({_id: id}).exec();
  if (!message){
    res.status(404).send('message not found');
    return;
  }
  res.locals.message = message.toJSONClient();

  if (message.ninja && message.ninja.point && message.score && message.score >= message.ninja.point && message.ninja.revealer && message.ninja.revealer.length){
    message.ninja.revealer.forEach(r => {
      revealer[r._id.toString()] = r;
      rs.push(r._id);
    });
    const rs = await User.find({
      _id: {$in: rs}
    }).select({
      _id: 1,
      name: 1,
      displayName: 1,
      avatarModified: 1
    }).exec();
    if (rs && rs.length){
      const revealers = [];
      rs.forEach(r => {
        var o = revealer[r._id.toString()];
        revealers.push({
          name: r.name,
          displayName: r.displayName,
          avatarMiniPath: r.avatarMiniPath,
          point: o.point
        });
      });
      revealers.sort((a, b) => a.point == b.point ? 0 : a.point > b.point ? -1 : 1);
      res.locals.message.revealers = revealers;
    }
  }
  res.renderDynamic('single');
};

export async function post(req, res){
  if (!req.user){
    res.status(403).send('please login');
    return;
  }
  const message = new Message(JSON.parse(req.body.body));
  message._id = new mongoose.Types.ObjectId();

  message.tags = wl2c.uniqueAndLimitTags(message.tags);
  if (!message.tags.length){
    message.tags = undefined;
  }

  message.user = req.user;
  const verifyPoint = pt => {
    switch(parseInt(pt, 10)){
      case 10:
        return 50;
      case 30:
        return 500;
      case 100:
        return 3000;
    }
    return 0;
  }
  const ninjaPoint = verifyPoint(req.body.pt);
  if (!ninjaPoint){
    message.ninja = undefined;
  } else {
    message.ninja.point = ninjaPoint;
    message.ninja.revealer = [];
  }
  if (req.files.file){
    await message.saveFile(req.files.file[0].path, req.files.file[0].originalname);
  } else {
    message.file = undefined;
  }
  await message.save();
  req.user.posted();
  await req.user.save();
  res.json(message.toJSONClient());
  message.notifyMessage();
};

export async function log(req, res){
  let from = new Date(req.query.from);
  if (isNaN(from.getTime())){
    from = new Date();
  }
  from.setHours(0);
  from.setMinutes(0);
  from.setSeconds(0);
  from.setMilliseconds(0);

  let to = new Date(req.query.to);
  if (isNaN(to.getTime())){
    to = new Date();
  }
  to.setHours(0);
  to.setMinutes(0);
  to.setSeconds(0);
  to.setMilliseconds(0);

  const tags = wl2c.decodeBase64Tag(req.query.t);
  const q = {
    created: {$gte: from, $lt: new Date(to.getTime() + 24*60*60*1000)},
    tags: tags.length ? {'$in': tags} : null
  };

  if (req.query.user) {
    const user = await User.findOne({name: req.query.user}).exec();
    if (!user) {
      return res.status(404).send('not found');
    }
    q['user._id'] = user._id;
    q['ninja.point'] = null;
  }

  const COUNT = 200;
  const messages = await Message.find(q).sort('created')
    .skip(COUNT * (parseInt(req.query.p, 10) - 1))
    .limit(COUNT + 1)
    .exec();
  const r = [];
  messages.forEach(message => r.push(message.toJSONClient()));
  const nextId = messages.length > COUNT ? messages[messages.length - 1]._id : null;
  res.json({messages: r, nextId: nextId});
};

export async function stream(req, res){
  const asyncWrite = str => new Promise((resolve, reject) => res.write(str, err => err ? reject(err) : resolve()));
  const asyncWriteEvent = async o => {
    await asyncWrite('event: '+o.event+'\n');
    if (o.id) {
      await asyncWrite('id: '+o.id+'\n');
    }
    await asyncWrite('data: '+o.data+'\n');
  };
  const sendHeaders = () => res.writeHead(200, {
    'Content-Type': 'text/event-stream',
    'Cache-Control': 'no-cache',
    'Connection': 'keep-alive',
    'X-Accel-Buffering': 'no'
  });
  const COUNT = 50;
  const for3G = req.query['3g'] == '1';
  const tags = wl2c.decodeBase64Tag(req.query.t);
  let id = req.get('Last-Event-ID');
  let sent = 0;
  if (id){
    // continue
    id = new mongoose.Types.ObjectId(id);
    const lastMessage = await Message.findOne({_id: id}).exec();
    if (!lastMessage){
      res.status(404).send('message not found');
      return;
    }
    const messages = await Message.find({
      _id: {$ne: id},
      created: {$gte: lastMessage.created},
      tags: tags.length ? {$in: tags} : null
    }).sort('created').limit(COUNT+1).exec();
    sendHeaders();
    const len = Math.min(messages.length, COUNT);
    for(let i = 0; i < len; ++i){
      const message = messages[i];
      await asyncWriteEvent({
        event: 'message',
        id: message._id,
        data: JSON.stringify(message.toJSONClient())
      }, next);
      ++sent;
      await asyncWrite('\n');
    }
  } else {
    // latest
    const messages = await Message.find({
      tags: tags.length ? {$in: tags} : null
    }).populate('user._id').sort('-created').limit(COUNT).exec();
    sendHeaders();
    for(var i = messages.length - 1, j = 1; 0 <= i; --i, ++j){
      const message = messages[i];
      await asyncWriteEvent({
        event: 'message',
        id: message._id,
        data: JSON.stringify(message.toJSONClient())
      });
      ++sent;
      await asyncWrite('\n');
    }
  }
  if (sent && for3G){
    await asyncWrite('retry: 500\n\n');
    res.end();
    return;
  }

  const streaming = (req, res, tags) => new Promise((resolve, reject) => {
    const notifyId = Message.generateNotifyId();
    const notify = async e => {
      const o = e.detail;
      if (notifyId in o.sent){ return; }
      o.sent[notifyId] = true;
      await asyncWriteEvent(o);
      if (for3G){
        await asyncWrite('retry: 500\n\n');
        res.end();
      } else {
        await asyncWrite('\n');
      }
    }
    Message.registerNotify(tags, o => notify(o).catch(err => (res.end(), reject(err))));
    req.on('close', () => {
      Message.unregisterNotify(tags, notify);
      resolve();
    }, true);
  });
  await streaming(req, res, tags);
};
