import { fileURLToPath } from 'node:url';
import passport from 'passport';
import path from 'node:path';

import passportTwitter from 'passport-twitter';
import passportGoogle from 'passport-google-oauth';
import passportDiscord from 'passport-discord';

import {User, UserAlias} from '../models/user.js';
import {Message} from '../models/message.js';

import * as utils from '../utils.js';
import { promisify } from 'node:util';

const __dirname = path.dirname(fileURLToPath(import.meta.url));

async function authenticated(profile, photo) {
  return ({
    provider: profile.provider,
    id: profile.id,
    username: profile.username ? profile.username : profile.id,
    displayName: profile.displayName ? profile.displayName : profile.provider + profile.id,
    photo: photo,
  });
}

export function configure(app){
  app.use(passport.initialize());
  app.use(passport.session());
  passport.use(new passportTwitter.Strategy({
    consumerKey: app.get('twitter consumer key'),
    consumerSecret: app.get('twitter consumer secret'),
    callbackURL: app.get('protocol')+'://'+app.get('domain')+'/auth/twitter'
  }, function(token, tokenSecret, profile, done){
    authenticated(profile, profile.photos[0].value).then(u => done(null, u)).catch(done);
  }));
  passport.use(new passportGoogle.OAuth2Strategy({
    clientID: app.get('google client id'),
    clientSecret: app.get('google client secret'),
    callbackURL: app.get('protocol')+'://'+app.get('domain')+'/auth/google',
    scope: 'https://www.googleapis.com/auth/userinfo.profile'
  }, function(token, tokenSecret, profile, done){
    authenticated(profile, profile._json.picture).then(u => done(null, u)).catch(done);
  }));
  passport.use(new passportDiscord.Strategy({
    clientID: app.get('discord client id'),
    clientSecret: app.get('discord client secret'),
    callbackURL: app.get('protocol')+'://'+app.get('domain')+'/auth/discord',
    scope: 'identify',
  }, function(token, tokenSecret, profile, done){
    console.log(profile);
    authenticated(profile, profile.avatar ? 'https://cdn.discordapp.com/avatars/'+profile.id+'/'+profile.avatar+'.png' : null).then(u => done(null, u)).catch(done);
  }));
  passport.serializeUser(function(user, done){
    done(null, user._id);
  }); 
  passport.deserializeUser(function(obj, done){
    User.findOne({_id: obj}, done);
  });
};

export async function autoLogin(req, res){
  await User.loginByAutoLoginToken(req, res);
}

export async function auth(req, res){
  switch(req.query.mode){
    case 'login':
      if (req.user) {
        res.redirect('/');
        return;
      }
      res.clearCookie('connect');
    break;
    case 'connect':
      if (!req.user) {
        res.redirect('/');
        return;
      }
      res.cookie(
        'connect',
        req.user._id,
        {
          expires: new Date(Date.now() + 1000*60*60*2),
          httpOnly: true,
          secure: req.app.get('protocol') == 'https',
          signed: true
        }
      );
    break;
  }
  switch(req.params[0]){
    case 'twitter':
    case 'google':
    case 'discord':
      const asyncAuthenticate = (req, res, strategy) => new Promise((resolve, reject) => (passport.authenticate(strategy, (err, user, info) => err ? reject(err) : resolve([user, info]))(req, res, reject), res.statusCode == 302 ? reject() : undefined));
      const [userinfo, info] = await asyncAuthenticate(req, res, req.params[0]);
      if (!userinfo){
        res.redirect('/');
        return;
      }
      if (req.signedCookies.connect){
        if (req.signedCookies.connect != req.user._id) {
          throw new Error('unexpected connect state');
        }
        // 連携の追加
        await UserAlias.deleteOne({_id: userinfo.provider + userinfo.id}).exec();
        const userAlias = new UserAlias({
          _id: userinfo.provider+userinfo.id,
          user: req.user._id
        });
        await userAlias.save();
        res.clearCookie('connect');
        res.redirect('/connect');
      } else {
        // 通常のログイン
        const user = await User.findAndCreateUser(userinfo.provider, userinfo.id, userinfo.username, userinfo.displayName, userinfo.photo);
        const asyncLogin = promisify(req.login);
        await asyncLogin(req, user);
        await user.saveAutoLoginToken(req, res);
        res.redirect('/');
      }
      break;
    default:
      res.status(404).send('not found');
      break;
  }
};

export async function logout(req, res){
  if (!req.user){
    res.redirect('/');
    return;
  }
  const asyncLogout = req => new Promise((resolve, reject) => req.logout(err => err ? reject(err) : resolve()));
  await req.user.clearAutoLoginToken(req, res);
  await asyncLogout(req);
  res.redirect('/');
};

export async function profile(req, res){
  const user = await User.findOne({name: req.params[0]}).exec();
  if (!user){
    res.status(404).send('user not found');
    return;
  }
  res.renderDynamic('profile', {user: user});
};

export async function mini(req, res){
  const user = await User.findOne({name: req.params[0]}).exec();
  if (!user){
    res.status(404).send('image not found');
    return;
  }
  if (!user.avatarModified){
    res.sendFile(path.join(__dirname, '../public/images/default-mini.png'));
    return;
  }
  await utils.writeImage(req, res, user._id+'_96');
};

export async function thumb(req, res){
  const user = await User.findOne({name: req.params[0]}).exec();
  if (!user){
    res.status(404).send('image not found');
    return;
  }
  if (!user.avatarModified){
    res.sendFile(path.join(__dirname, '../public/images/default-thumb.png'));
    return;
  }
  await utils.writeImage(req, res, user._id.toString());
};

export async function settings(req, res){
  res.renderDynamic('settings', {});
};

export async function saveSettings(req, res){
  const old_name = req.user.name;
  if (req.files.avatar) {
    await req.user.saveAvatar(req.files.avatar[0].path);
  }
  req.user.name = req.body.name;
  req.user.displayName = req.body.displayName;
  await req.user.save();

  if (old_name != req.user.name){
    await Message.rename(req.user);
  }
  res.json({
    name: req.user.name,
    displayName: req.user.displayName,
    avatarMiniPath: req.user.avatarMiniPath
  });
};

export async function indexConnect(req, res){
  if (!req.user){
    res.redirect('/');
    return;
  }
  const r = await UserAlias.find({_id: /^(twitter|google|discord)/, user: req.user._id}).exec();
  const keys = {};
  for (let i = 0; i < r.length; ++i) {
    switch (r[i]._id.substring(0, 4)) {
    case 'twit':
      keys.twitter = 1;
      break;
    case 'goog':
      keys.google = 1;
      break;
    case 'disc':
      keys.discord = 1;
      break;
    }
  }
  res.renderDynamic('connect', { keys });
};
