import * as point from '../models/point.js';

export async function redir(req, res){
  res.render('redir', {});
};

export async function index(req, res){
  res.renderDynamic('index', {});
};

export async function help(req, res){
  res.locals.is_point_day = point.isPointDay();
  res.renderDynamic('help', {});
};

export async function log(req, res){
  res.renderDynamic('log', {});
};
