export default function configure(app){
  app.set('port', process.env.WL2_PORT || 3000);
  app.set('protocol', process.env.WL2_PROTOCOL || 'https');
  app.set('domain', process.env.WL2_DOMAIN || 'example.com');
  app.set('cookie secret', process.env.WL2_COOKIE_SECRET || 'your secret here');
  app.set('mongodb', process.env.WL2_MONGODB || 'mongodb://localhost/dbname');

  app.set('auto login keep days', process.env.WL2_AUTO_LOGIN_KEEP_DAYS || 30);

  //OAuth1.0a
  app.set('twitter consumer key', process.env.WL2_TWITTER_CONSUMER_KEY || 'foo');
  app.set('twitter consumer secret', process.env.WL2_TWITTER_CONSUMER_SECRET || 'bar');

  //OAuth2.0
  app.set('google client id', process.env.WL2_GOOGLE_CLIENT_ID || 'foo');
  app.set('google client secret', process.env.WL2_GOOGLE_CLIENT_SECRET || 'bar');

  //OAuth2.0
  app.set('discord client id', process.env.WL2_DISCORD_CLIENT_ID || 'foo');
  app.set('discord client secret', process.env.WL2_DISCORD_CLIENT_SECRET || 'bar');
}
