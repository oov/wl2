const pointDays = " 1/1 1/3 1/7 1/9 1/13 1/27 1/31 2/11 2/23 2/27 2/29 3/7 3/11 3/13 3/17 3/31 4/1 4/9 4/19 4/21 5/3 5/9 5/21 5/23 6/1 6/7 6/13 6/17 6/19 7/1 7/9 7/19 7/27 8/9 8/11 8/21 8/23 8/27 8/29 9/7 9/11 9/19 9/29 10/9 10/13 10/19 10/21 10/31 11/3 11/9 11/17 11/23 11/29 12/1 12/13 12/17 12/23 12/29 12/31 ";
export function isPointDay(date){
  if (!date) date = new Date();
  return pointDays.indexOf(' ' + (date.getMonth()+1) + '/' + (date.getDate()) + ' ') != -1;
}

export function getNewPostPoint(date){
  return isPointDay(date) ? 200 : 100;
}

export function getDailyPostPoint(date){
  return isPointDay(date) ? 20 : 10;
}
