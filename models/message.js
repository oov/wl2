import {setMaxListeners} from 'node:events';
import mongoose from 'mongoose';

import { User } from '../models/user.js';
import * as utils from '../utils.js';

class CustomEvent extends Event {
  constructor(message, data) {
    super(message, data);
    this.detail = data.detail;
  }
}

const messageNotifier = new EventTarget();
setMaxListeners(0, messageNotifier);

const MessageSchema = new mongoose.Schema({
  message: {
    type: String,
    validate: [v => v.length > 0 && v.length < 10000, "empty"]
  },
  user: {
    _id: {
      type: mongoose.Schema.Types.ObjectId,
      index: true
    },
    name: String,
    displayName: String,
    avatarModified: Date
  },
  ninja: {
    point: {
      type: Number,
      default: 0
    },
    revealer: {
      type: [{
        _id: {
          type: mongoose.Schema.Types.ObjectId,
          index: true
        },
        point: {
          type: Number,
          default: 0
        }
      }]
    }
  },
  file: {
    type: String,
    validate: [v => v.length < 128, "filename length must be 127 characters or less"],
  },
  image: {
    type: Boolean
  },
  tags: [String],
  score: Number,
  created: {
    type: Date,
    default: Date.now
  }
});
MessageSchema
  .index({created: -1})
  .index({created: 1})
  .index({tags: 1});

MessageSchema.virtual('filePath').get(function(){
  if (this.file){
    return '/file/'+this._id+'/'+encodeURIComponent(this.file);
  } else {
    return null;
  }
});

MessageSchema.virtual('fileThumbPath').get(function(){
  if (this.image){
    return '/thumb/'+this._id+'/'+encodeURIComponent(this.file);
  } else {
    return null;
  }
});

MessageSchema.virtual('user.avatarPath').get(function(){
  if (!this.user.avatarModified){
    return '/images/default.png';
  } else {
    return '/!'+this.user.name+'.thumb?'+this.user.avatarModified.getTime();
  }
});
MessageSchema.virtual('user.avatarMiniPath').get(function(){
  if (!this.user.avatarModified){
    return '/images/default-mini.png';
  } else {
    return '/!'+this.user.name+'.mini?'+this.user.avatarModified.getTime();
  }
});

let notifyIdCounter = 0;
MessageSchema.statics.generateNotifyId = function(){
  return ++notifyIdCounter;
};

MessageSchema.statics.registerNotify = function(tags, callback){
  if (tags.length){
    for (let i = 0; i < tags.length; i++) {
      messageNotifier.addEventListener('_' + tags[i], callback);
    }
  } else {
    messageNotifier.addEventListener('_', callback);
  }
};

MessageSchema.statics.unregisterNotify = function(tags, callback){
  if (tags.length){
    for (let i = 0; i < tags.length; i++) {
      messageNotifier.removeEventListener('_' + tags[i], callback);
    }
  } else {
    messageNotifier.removeEventListener('_', callback);
  }
};

MessageSchema.statics.notify = function(tags, data){
  if (tags && tags.length){
    tags.forEach(function(tag){
      messageNotifier.dispatchEvent(new CustomEvent('_' + tag, {detail: data}));
    });
  } else {
    messageNotifier.dispatchEvent(new CustomEvent('_', {detail: data}));
  }
};

MessageSchema.statics.rename = async function(user){
  await Message.updateMany({
    'user._id': user._id
  }, {
    $set: {'user.name': user.name}
  }, {multi: true}).exec();
};

MessageSchema.statics.score = async function(user_id, message_id, pt){
  // ポイント支払い先メッセージを検索
  const message = await Message.findById(message_id, {tags: 1, user: 1, score: 1, ninja: 1}).exec();
  if (message.user._id.equals(user_id)){
    throw 'could not be processed';
  }
  // 支払い元からポイントを徴収
  if (!await User.pay(user_id, -pt)) {
    throw 'point is short';
  }
  // undefined 回避
  if (!message.score){
    message.score = 0;
  }
  // 忍者突破までに必要なポイント数
  const ninjaRemain = message.ninja && message.ninja.point ? Math.max(message.ninja.point - message.score, 0) : 0;
  if (ninjaRemain == 0){
    // 通常のポイント加算
    const r = await Message.updateOne({_id: message_id}, {$inc: {score: pt}});
    if (!r.modifiedCount) {
      // 支払いに失敗した
      await User.pay(user_id, pt);
      throw 'Your payment can not be completed.';
    }
  } else {
    // 忍者
    // エントリがないかも知れないので先に追加
    await Message.updateOne({
      _id: message_id,
      'ninja.revealer._id' : {
        $ne : user_id
      }
    }, {
      $push: {
        'ninja.revealer' : {
          _id: user_id,
          point: 0
        }
      }
    });
    // ポイントを加算
    const r = await Message.updateOne({
      _id: message_id,
      'ninja.revealer._id': user_id
    }, {
      $inc: {
        score: pt,
        'ninja.revealer.$.point': Math.min(ninjaRemain, pt)
      }
    });
    if (!r.modifiedCount) {
      // 支払いに失敗した
      await User.pay(user_id, pt);
      throw 'Your payment can not be completed.';
    }
  }
  const apt = pt - Math.min(ninjaRemain, pt);
  if (apt) {
    await User.pay(message.user._id, apt);
    message.score += pt;
  }
  return message;
};

MessageSchema.statics.post = function(user, messagedone){
};

MessageSchema.methods.saveFile = async function(path, name){
  const fileinfo = await utils.fileinfo(path);
  if (fileinfo.width && fileinfo.height){
    if (fileinfo.width > 10000 || fileinfo.height > 10000){
      throw 'upload image too large';
    }
    await utils.resize(fileinfo.path, fileinfo.mime, this._id+'_256', 256);
  }
  await utils.saveAndRemove(fileinfo.path, fileinfo.mime, this._id.toString());
  this.file = name;
  this.image = !!(fileinfo.width && fileinfo.height);
};

MessageSchema.methods.notifyMessage = function(){
  Message.notify(this.tags, {
    sent: {},
    event: 'message',
    id: this._id,
    data: JSON.stringify(this.toJSONClient())
  });
};

MessageSchema.methods.notifyScore = function(){
  Message.notify(this.tags, {
    sent: {},
    event: 'score',
    data: JSON.stringify({_id: this._id, score: this.score})
  });
};

MessageSchema.methods.notifyRevealed = function(){
  Message.notify(this.tags, {
    sent: {},
    event: 'revealed',
    id: this._id,
    data: JSON.stringify({_id: this._id, score: this.score, ninja: this.ninja.point, user: this.toJSONClientUser()})
  });
};

function tagsFilter(tags){
  var r = [];
  if (!tags) {
    return r;
  }
  tags.forEach(function(tag){
    if (tag.substring(tag.length - 7) == ':secret') return;
    r.push(tag);
  });
  return r;
}

MessageSchema.methods.toJSONClientUser = function(){
  if (this.ninja && this.ninja.point && (!this.score || this.score < this.ninja.point)){
    //暴かれていない忍者
    return {};
  }
  //暴かれた忍者と通常投稿
  return {
    displayName: this.user.displayName,
    name: this.user.name,
    avatarMiniPath: this.user.avatarMiniPath
  };
}

MessageSchema.methods.toJSONClient = function(){
  var obj = {
    _id: this._id,
    created: this.created,
    message: this.message,
    file: this.file,
    filePath: this.filePath,
    fileThumbPath: this.fileThumbPath,
    score: this.score,
    tags: tagsFilter(this.tags),
  };

  obj.user = this.toJSONClientUser();
  if (this.ninja && this.ninja.point && this.score && this.score >= this.ninja.point){
    obj.ninja = this.ninja.point;
  }

  return obj;
};

export const Message = mongoose.model('Message', MessageSchema);
