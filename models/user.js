import mongoose from 'mongoose';

import * as utils from '../utils.js';
import * as point from './point.js';

const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    unique: true,
    validate: [
      {validator: function(v){ return typeof v === "string" && v.length > 0 && v.length < 64; }, msg: "required"},
      {validator: function(v){ return typeof v === "string" && v.match(/^[a-zA-Z0-9_]+$/); }, msg: "can use only [a-zA-Z0-9_]."}
    ]
  },
  displayName: {
    type: String,
    validate: [function(v){ return typeof v === "string" && v.length > 0 && v.length < 64; }, "required"]
  },
  point: {
    type: Number,
    default: 0
  },
  created: {
    type: Date,
    default: Date.now
  },
  lastPosted: {
    type: Date,
    default: null
  },
  avatarModified: {
    type: Date,
    default: null
  }
});

const UserAliasSchema = new mongoose.Schema({
  _id: {
    type: String
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    index: true
  },
  period: {
    type: Date,
    expires: 0
  }
});

UserSchema.statics.pay = async function(user_id, pt){
  if (pt < 0) {
    //ユーザーからポイントを引く
    const r = await User.updateOne(
      {_id: user_id, point: {$gte: -pt}},
      {$inc: {point: pt}}
    );
    return r.modifiedCount;
  } else if (pt > 0) {
    //ユーザーにポイントを与える
    const r = await User.updateOne(
      {_id: user_id},
      {$inc: {point: pt}}
    );
    return r.modifiedCount;
  }
  return 1;
};

UserSchema.methods.pay = async function(pt){
  return await User.pay(this._id, pt);
};

UserSchema.methods.saveAvatar = async function(path){
  const fileinfo = await utils.fileinfo(path);
  if (!fileinfo.width || !fileinfo.height){
    throw 'unsupported image file format';
  }
  if (fileinfo.width > 10000 || fileinfo.height > 10000){
    throw 'upload image too large';
  }
  await utils.crop(fileinfo.path, fileinfo.mime, this._id+'_96', 96);
  await utils.crop(fileinfo.path, fileinfo.mime, this._id.toString(), 256);
  this.avatarModified = Date.now();
  await utils.remove(fileinfo.path);
};

UserSchema.methods.posted = function(){
  if (this.lastPosted){
    const o = new Date(this.lastPosted);
    const n = new Date();
    o.setHours(0);
    o.setMinutes(0);
    o.setSeconds(0);
    o.setMilliseconds(0);
    n.setHours(0);
    n.setMinutes(0);
    n.setSeconds(0);
    n.setMilliseconds(0);
    if (o.getTime() != n.getTime()){
      this.point += point.getDailyPostPoint();
      this.increment();
    }
  } else {
    this.point += point.getNewPostPoint();
    this.increment();
  }
  this.lastPosted = new Date();
};

UserSchema.methods.saveAutoLoginToken = async function(req, res){
  const token = utils.generateToken(76);
  const expires = new Date(Date.now() + 1000*60*60*24*req.app.get('auto login keep days'));

  const userAlias = new UserAlias({
    _id: 'autoLogin'+token,
    user: this._id,
    period: expires
  });
  await userAlias.save();
  res.cookie(
    'autoLogin',
    token,
    {
      expires: expires,
      httpOnly: true,
      secure: req.app.get('protocol') == 'https',
      signed: true
    }
  );
};

UserSchema.methods.clearAutoLoginToken = async function(req, res){
  if (!req.signedCookies.autoLogin){
    return;
  }
  await UserAlias.deleteOne({
    _id: req.signedCookies.autoLogin,
    user: this._id
  });
  res.clearCookie('autoLogin');
};


UserSchema.statics.loginByAutoLoginToken = async function(req, res){
  if (!req.signedCookies.autoLogin || req.user){
    return;
  }
  const userAlias = await UserAlias
    .findOne({_id: 'autoLogin'+req.signedCookies.autoLogin})
    .populate('user')
    .exec();
  if (!userAlias){
    //アカウントが見つけられなかったが失敗はしていない
    res.clearCookie('autoLogin');
    return;
  }
  const asyncLogin = (req, user) => new Promise((resolve, reject) => req.login(user, err => err ? reject(err) : resolve(user)));
  const user = await asyncLogin(req, userAlias.user);
  await user.clearAutoLoginToken(req, res);
  await user.saveAutoLoginToken(req, res);
};

UserSchema.statics.assignUser = async function(loggedUser, provider, id){
  await UserAlias.deleteMany({_id: provider+id}).exec();
  const userAlias = new UserAlias({
    _id: provider+id,
    user: loggedUser._id,
  });
  await userAlias.save();
};

UserSchema.statics.findAndCreateUser = async function(provider, id, username, displayName, photo){
  /** @type function(): Promise<[boolean, User]> */
  const findAndCreate = async function() {
    let userAlias = await UserAlias.findOne({_id: provider+id}).populate('user').exec();
    if (userAlias){
      return [false, userAlias.user];
    }
    const user = new User({
      name: username,
      displayName: displayName
    });
    try {
      user.name = provider+id;
      await user.save();
    } catch (err) {
      if (err.code != 11000) {
        throw err;
      }
      // 名前が衝突しているっぽいので一応回避策
      user.name = provider+id+'_'+(Math.random()*0xffff).toFixed(0);
      await user.save();
    }
    userAlias = new UserAlias({
      _id: provider+id,
      user: user._id
    });
    await userAlias.save();
    return [true, user];
  };
  const [created, user] = await findAndCreate();
  if (created && photo) {
    // 新規で、かつアバター用画像があるなら初期画像として採用する
    await user.saveAvatar(await utils.downloadToTempDir(photo));
    await user.save();
  }
  return user;
};

UserSchema.virtual('avatarPath').get(function(){
  if (!this.avatarModified){
    return '/images/default.png';
  } else {
    return '/!'+this.name+'.thumb?'+this.avatarModified.getTime();
  }
});
UserSchema.virtual('avatarMiniPath').get(function(){
  if (!this.avatarModified){
    return '/images/default-mini.png';
  } else {
    return '/!'+this.name+'.mini?'+this.avatarModified.getTime();
  }
});
export const User = mongoose.model('User', UserSchema);
export const UserAlias = mongoose.model('UserAlias', UserAliasSchema);
