import { fileURLToPath } from 'node:url';
import os from 'node:os';
import path from 'node:path';
import process from 'node:process';

import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import connectMongo from 'connect-mongo';
import errorHandler from 'errorhandler';
import express from 'express';
import expressSession from 'express-session';
import multer from 'multer';
import methodOverride from 'method-override';
import mongoose from 'mongoose';
import morgan from 'morgan';
import serveFavicon from 'serve-favicon';
import stylus from 'stylus';

import configure from './configure.js';
import * as message from './routes/message.js';
import * as routes from './routes/index.js';
import * as user from './routes/user.js';

const asyncMiddleware = (fn) => (req, res, next) => fn(req, res).then(next).catch(next);
const asyncHandler = (fn) => (req, res, next) => fn(req, res).catch(next);

const app = express();

configure(app);

const __dirname = path.dirname(fileURLToPath(import.meta.url));

if (process.env.NODE_ENV === 'development') {
  mongoose.set('debug', true);
}

mongoose.set('strictQuery', true);
mongoose.connect(app.get('mongodb'));

app.set('views', __dirname + '/views');
app.set('view engine', 'pug');
app.use(morgan('dev'));
app.use(serveFavicon(path.join(__dirname, 'public/favicon.ico')));
express.static.mime.define({'text/javascript': ['cjs']});
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(methodOverride());
app.use(cookieParser(app.get('cookie secret')));
app.use(expressSession({
  secret: app.get('cookie secret'),
  saveUninitialized: false,
  resave: false,
  proxy: true,
  cookie: {
    secure: app.get('protocol') == 'https'
  },
  store: connectMongo.create({
    mongoUrl: app.get('mongodb'),
    touchAfter: 24 * 3600,
  })
}));
user.configure(app);
app.use(asyncMiddleware(user.autoLogin));
app.use(asyncMiddleware(async function(req, res){
  res.renderDynamic = function(viewName, vars){
    res.locals._user = req.user;
    res.locals._viewOnly = !!req.query.angular;
    res.locals._view = viewName;
    res.render(viewName, vars, function(err, html){
      if (err){
        res.status(500).send('internal server error');
        console.log(err);
        console.log(err.stack);
        return;
      }
      res.locals._content = html;
      res.render('chrome', vars);
    });
  };
}));
app.use(stylus.middleware({
  src: __dirname + '/views',
  dest: __dirname + '/public'
}));

const uploadMiddleware = multer({ dest: path.join(os.tmpdir(), 'wl2-tmp-upload') });

app.use((err, req, res, next) => {
  console.error(err);
  res.status(500).send('internal server error');
});

app.get('/redir', asyncHandler(routes.redir));

app.get('/', asyncHandler(routes.index));
app.get('/index', asyncHandler(routes.index));
app.get('/help', asyncHandler(routes.help));
app.get('/log', asyncHandler(routes.log));
app.get(/\/message\/(stream|log)$/, asyncHandler(async (req, res) => await message[req.params[0]](req, res)));
app.get(/\/(file|thumb)\/([a-fA-F0-9]+)\/.*$/, asyncHandler(message.file));
app.post('/message/post', uploadMiddleware.fields([{name:'file', maxCount:1}]), asyncHandler(message.post));
app.post('/message/score', asyncHandler(message.score));
app.get(/^\/![a-zA-Z0-9_]*\/([a-fA-F0-9]+)$/, asyncHandler(message.single));

app.get('/settings', asyncHandler(user.settings));
app.post('/settings/save', uploadMiddleware.fields([{name:'avatar', maxCount:1}]), asyncHandler(user.saveSettings));
app.get(/^\/auth\/([a-zA-Z0-9_]+)?$/, asyncHandler(user.auth));
app.get('/connect', asyncHandler(user.indexConnect));
app.get('/logout', asyncHandler(user.logout));
app.get(/^\/!([a-zA-Z0-9_]+)(?:\.(thumb|mini))?$/, asyncHandler(async (req, res) => await user[req.params[1] ? req.params[1] : 'profile'](req, res)));

if (process.env.NODE_ENV === 'development') {
  app.use(errorHandler);
}

const server = app.listen(app.get('port'), () => {
  console.log("Express server listening on port " + app.get('port'));
});

process.on('SIGINT', () => {
	console.log('SIGINT');
  shutdown();
});

process.on('SIGTERM', () => {
  console.log('SIGTERM');
  shutdown();
});

const sockets = {};
let socketId = 0;
server.on('connection', socket => {
  const id = ++socketId;
  sockets[id] = socket;
  socket.once('close', () => delete sockets[id]);
});

function shutdown() {
  waitSockets(10);
  server.close(err => {
    if (err) {
      console.error(err);
      process.exitCode = 1;
    }
    process.exit();
  });
}

function waitSockets(counter) {
  if (counter > 0) {
    return setTimeout(waitSockets, 1000, counter - 1);
  }
  console.log("graceful shutdown failed.");
  for (const id in sockets) {
    sockets[id].destroy();
  }
}
